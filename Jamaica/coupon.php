<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Indian Halal Chinese Restaurant in New York, Deshi Chinese Restaurant in New York, Halal Restaurant in Jamaica, New York</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<section>
        <header>
            <a href="#"><img src="img/header_bg_logoimg.jpg" alt="logo" class="img-responsive logo"/></a>
        </header>
        <nav class="navbar menu">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="index.html">Home <span class="sr-only">(current)</span></a></li>
                <li><a href="about.html">About Us</a></li>
                <li><a href="menu.html">Menu</a></li>
                <li><a href="catering.html">Catering</a></li>
                <li><a href="gallery.html">Gallery</a></li>
                <li><a href="Video.html">Video</a></li>
                <li><a href="contact.html">Contact</a></li>
                <li><a href="coupon.php">Coupon</a></li>
                <li><a href="comments.php">Comments</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <section>
        	<div class="container">
            	<div class="row">
                	<div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-3 coupon">
                    	<p>Coupon</p>
                        <img src="img/manu-btm.png" class="img-responsive" />
                        <ul>
                        
                        </ul>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 confidential">
                        <p class="text-center">To recieve coupon please join our customer list</p>
                        <p class="text-center">We keep your info confidential</p>
                        
                        <form action="coupon.php" class="form-horizontal" method="post">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 col-xs-4 control-label">First Name:</label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="text" name="first_name" class="form-control" id="inputEmail3" placeholder="First Name">
                                </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 col-xs-4 control-label">Last Name:</label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="text" name="last_name" class="form-control" id="inputPassword3" placeholder="Last Name">
                                </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 col-xs-4 control-label">Mailing Address:</label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="text" name="address" class="form-control" id="inputPassword3" placeholder="Mailing Address">
                                </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 col-xs-4 control-label"></label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="" class="form-control" id="inputPassword3" placeholder="">
                                </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 col-xs-4 control-label">Email Address:</label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="text" name="email_Address" class="form-control" id="inputEmail3" placeholder="Email Address">
                                </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 col-xs-4 control-label">Phone:</label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="number" name="number" class="form-control" id="inputEmail3" placeholder="Phone">
                                </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 col-xs-4 control-label">Cell Phone:</label>
                                <div class="col-sm-8 col-xs-8">
                                  <input type="number" name="number" class="form-control" id="inputEmail3" placeholder="Cell Phone">
                                </div>
                          </div>
                          <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                          </div>
                          
                          
                        </form>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
                    	<img src="img/button.png" class="img-responsive tellFriend" />
                    </div>
                </div>
            </div>
        </section>
    </section>
    <footer>
        <div class="container footer-top">
            <div class="row">
            	<div class="col-md-4 col-sm-4">
                	<img src="img/tree_man.gif" class="img-responsive tree" alt="Sagar Chinese"  />          
                </div>
                <div class="col-md-8 col-sm-8 col-xs-11 newyork">                   	
                    <img src="img/sagar_add.gif" class="img-responsive sagarAdd" alt="Sagar Chinese" />
                    <ul class="nav desiChinese">
                      <li><a href="http://sagarchinese.com/blog" target="_blank">Blog</a></li>
                      <li><a target="_blank"  href="http://maps.google.com/maps/place?hl=en&gs_upl=&bav=on.2,or.r_gc.r_pw.,cf.osb&biw=1280&bih=685&um=1&ie=UTF-8&q=sagar+chinese&fb=1&hq=sagar+chinese&cid=12706880072885263699&ei=b6QWT_KqD4bxrQfKsZRK&sa=X&oi=local_result&ct=photo-link&cd=4&resnum=1&sqi=2&ved=0CAwQnwIoAzAA">Review  </a></li>        
        			  <li><a href="#">Delivery Policy </a>  </li>
                      <li><a href="comments.php">How Are We Doing?</a>  </li>
                    </ul>
                    <ul class="nav followUs">
                        <li><img src="img/follow_us.gif" alt="" class="img-responsive" /></li>
                        <li><a href="http://www.facebook.com/sagarchinese" target="_blank"><img src="img/facebook.png" alt="" class="img-responsive" /></a></li>
                        <li><a href="https://twitter.com/sagarchinese" target="_blank"><img src="img/twttwe.png" alt="" class="img-responsive" /></a></li>
                        <li><a href="https://www.youtube.com/watch?v=UiOMq4XQCqE"><img src="img/youtube.png" alt="" class="img-responsive" /></a></li>
                    </ul>
                    <p>Copyright &copy; 2010-2014. SagarChinese.com. All rights reserved.</p>                                

                </div>                    
            </div> 
                             
        </div>        	       
    </footer>
    <div class="container-fluid bottomImg">        
          <img src="img/footer.jpg" class="img-responsive" />       
    </div>
    
            
        

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
  </body>
</html>